sayi1 = input("1. Sayı: ")
sayi2 = input("2. Sayı: ")

if sayi1.isdecimal() and sayi2.isdecimal():
    sayi1 = int(sayi1)
    sayi2 = int(sayi2)

    islem = input("İşlem Giriniz (+,-,*,/,**): ")

    if islem == "+":
        print("Sonuç:", sayi1+sayi2)
    elif islem == "-":
        print("Sonuç:", sayi1-sayi2)
    elif islem == "*":
        print("Sonuç:", sayi1*sayi2)
    elif islem == "/":
        if sayi2 != 0:
            print("Sonuç:", sayi1/sayi2)
        else:
            print("0'a bölme işlemi yapılamaz!")
    elif islem == "**":
        print("Sonuç:", sayi1**sayi2)
else:
    print("Sadece sayı girebilirsiniz!")
