#list(musteri_sozlugu.keys()).count(musteri_adi):

def sozlukte_var_mi(sozluk_ismi, aranan_kelime):
    return bool(list(sozluk_ismi.keys()).count(aranan_kelime))

menu_secimi = ""
musteri_sozlugu = {}
urun_sozlugu = {}
print("Erdal Bakkal programına hoş geldiniz")
while menu_secimi != "q":
	print("İşlemler: ")
	print("1.Ürün Girişi\n2.Müşteri Girişi\n3.Müşteri Borç listeleme\n4.Ürün Listeleme\n5.Ürün Satışı\n6.Borç Ödeme\n7.Çıkış")
	menu_secimi = input("Yapmak istediğiniz işlemi seçiniz: ")
	if menu_secimi == "1":
		print("Girisi durdurmak icin tamam yazınız")
		while True:
			urun_adi = input("Ürün adı giriniz: ")
			urun_fiyatı = input("Ürün fiyatı giriniz: ")
			if urun_adi.lower() == "tamam" or urun_fiyatı.lower() =="tamam":
				break
			else:
				if sozlukte_var_mi(urun_sozlugu, urun_adi):
					print("#####################")				
					print("Bu ürün kayıtlıdır fiyatı güncellenecektir")
					print("#####################")
				if urun_fiyatı.isdecimal():
					urun_sozlugu[urun_adi] = int(urun_fiyatı)
				else:
					print("#####################")
					print("Lütfen ürün fiyatını düzgün giriniz")
					print("#####################")
	elif menu_secimi == "2":
		print("Girisi durdurmak icin tamam yazınız")
		while True:
			musteri_adi = input("Musteri adi giriniz: ")
			if musteri_adi.lower() == "tamam":
				break
			elif list(musteri_sozlugu.keys()).count(musteri_adi):
				print("#####################")				
				print("Bu müşteri kayıtlıdır")
				print("#####################")
			else:
				musteri_sozlugu[musteri_adi] = 0
	elif menu_secimi == "3":
		if len(musteri_sozlugu) == 0:
			print("#####################")
			print("Hiç müşterimiz yok :(")
			print("#####################")
		else:
			print("#####################")
			for k,v in musteri_sozlugu.items():
				print(k+" : "+str(v))
			print("#####################")
	elif menu_secimi == "4":
		if len(urun_sozlugu) == 0:
			print("#####################")
			print("Hiç ürünümüz yok :(")
			print("#####################")
		else:
			print("#####################")
			for k,v in urun_sozlugu.items():
				print(k+" : "+str(v))
			print("#####################")
	elif menu_secimi == "5":
		print("Girisi durdurmak icin musteri adına tamam yazınız.")
		while True:
			musteri_adi = input("Musteri adı belirtiniz: ")
			if musteri_adi == "tamam":
				break
			elif list(musteri_sozlugu.keys()).count(musteri_adi) == 0:
				print("#####################")
				print("Böyle bir müşterimiz mevcut değildir")
				print("#####################")
			else:
				urun_adi = input("Urun adı giriniz: ")
				if list(urun_sozlugu.keys()).count(urun_adi) == 0:
					print("#####################")
					print("Böyle bir ürünümüz mevcut değildir")
					print("#####################")
				else:
					urun_adedi = input("Urun adedi giriniz: ")
					if urun_adedi.isdecimal():
						musteri_sozlugu[musteri_adi] += urun_sozlugu[urun_adi] * int(urun_adedi)
					else:
						print("#####################")
						print("Urun adedini yanlış girdiniz")
						print("#####################")
	elif menu_secimi == "6":
		print("Girisi durdurmak icin tamam yazınız.")
		while True:
			musteri_adi = input("Musteri adini giriniz: ")
			if musteri_adi == "tamam":
				break
			else:
				if list(musteri_sozlugu.keys()).count(musteri_adi) == 0:
					print("#####################")
					print("Böyle bir müşterimiz mevcut değildir")
					print("#####################")
				else:
					print("Borç miktarı: "+ str(musteri_sozlugu[musteri_adi]))
					tutar = input("Odenecek miktarı giriniz: ")
					if tutar.isdecimal():
						if int(tutar) > musteri_sozlugu[musteri_adi]:
							print("#####################")
							print("Borçtan fazla miktar girdiniz")
							print("#####################")
						else:
							musteri_sozlugu[musteri_adi] -= int(tutar)
							print("#####################")
							print("Kalan bakiye: " + str(musteri_sozlugu[musteri_adi]))
							print("#####################")
					else:
						print("#####################")
						print("Miktarı yanlış girdiniz")
						print("#####################")
	elif menu_secimi == "7":
		print("#####################")
		print("YINE BEKLERIZ")
		print("#####################")
		break

	else:
		print("#####################")
		print("Yanlış giriş yaptınız")
		print("#####################")
