# Dictionary
# ----------
# dict(key1=value, key2=value, key3=value)
# dict([(), (), ()])
# len(dict)
# str(dict)
# del dict[...]
# del dict
# sorted(dict)
# list(dict)
# dict.clear()
# dict.copy()
# dict.fromkeys(seq, value)
# dict.get(key, default=None)
# dict.items()
# dict.keys()
# dict.setdefault(key, default=None)
# dict.update(dict2)
# dict.values()
