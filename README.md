# ornek-projeler

Uygulama1: Kullanıcının girdiği iki sayıyı, yine kullanıcının istediği toplama/çıkarma/çarpma/bölme/üssünü alma işlemlerini yapıp sonucu yazdırma

Uygulama2: Uygulama1'deki işlemi sadece 2 sayı için değil kullanıcının istediği kadar girdiği sayı için yapan program

Notlar:
    - Kullanıcının girdiği değere güvenmeden, hata oluşmadan çalışmasını sağlayın.
    - Uygulama2 için list kullanabilirsiniz. Listeye eleman eklemek için .append() kullanılabilir.

