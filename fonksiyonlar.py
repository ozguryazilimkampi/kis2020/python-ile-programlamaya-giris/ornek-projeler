#Fonksiyonlar

#Kullanıcıdan veri alan fonksiyon, çıktı olarak True, False verecek

def soru_sor(kullanici_sorusu="Devam etmek ister misiniz?"):
    cevap = input(kullanici_sorusu)
    if cevap == "e":
        return True
    else:
        return False

#print(soru_sor())
#print(soru_sor("Nasılsın"))
#if soru_sor() == True:
#    print("Devam ediliyor")
#else:
#    print("Bitti")

#2 sayiyi toplayan fonksiyon
def topla(sayi1, sayi2):
    "Ben bir yardımcıyım, 2 sayıyı toplarım"
    print("Sayi 1 ==>", sayi1)
    print("Sayi 2 ==>", sayi2)
    toplam = sayi1 + sayi2
    return toplam

#print(topla(sayi2=3,sayi1=5))

#print(help(topla))

#Verilen sayilari toplayan fonksiyon
def sinirsiz_topla(*sayilar):
    toplam = 0
    for sayi in sayilar:
        toplam += sayi
    return toplam

#print(sinirsiz_topla(1,2,3,4,5,6))

toplam = sinirsiz_topla(1,2,3,4,5)
print(toplam)
print("----")

ben_global = "Global"
def ben_local(x):
    x = "Local" 
    return x

#print(ben_global)
#print(ben_local(ben_global))
#ben_local(ben_global)
#print(ben_global)

degistir_beni = "Selam"
def global_degisken_degistirme():
    global degistir_beni
    degistir_beni = "Merhaba"

print(degistir_beni)
global_degisken_degistirme()
print(degistir_beni)

