from urllib import request
from json import loads

adres = "http://api.orhanaydogdu.com.tr/deprem/live.php"

istek = request.urlopen(adres)
sonuc = istek.read()
encoding = istek.info().get_content_charset('utf-8')
depremler = loads(sonuc.decode(encoding))

for deprem in depremler['result']:
    print(deprem['date'], deprem['lokasyon'], sep=':')
    
