# -*- coding: utf-8 -*-
#Soru
#Kullanıcıdan girdi alalım, girdiği değerlerden bir dizi oluşturalım.
#Girdinin sonlanması için küçük 'q' ifadesiyle karşılaştıralım. 
#Örneğin kullanıcı_dizisi = [1,2,3,3]
#Sonrasında oluşan dizide elemanlar aynı ise çıktı olarak "True"
#Farklı ise "False" çıktısı verelim

#Örnek dizi ve çıktıları
#[1,2,3]    ==> False
#[1,1,1]    ==> True
#['a', 'a'] ==> True
#['a', 'b'] ==> False
#[]         ==> True

#While kullanarak Çözümü
kullanici_dizisi = []

while True:
    kullanici_girdisi = input("Merhaba Dünyalı senden gelecek girdiyi bekliyorum, ama çıkmak istersen 'q' yazabilirsin: ")
    if kullanici_girdisi.lower() == 'q':
            break
    if kullanici_girdisi.isdecimal() == True:
        kullanici_dizisi.append(int(kullanici_girdisi))
    else:
        kullanici_dizisi.append(kullanici_girdisi)

if(len(kullanici_dizisi) == 0):
    print(kullanici_dizisi," ==> True")
else:
    if(kullanici_dizisi.count(kullanici_dizisi[0]) == len(kullanici_dizisi)):
        print(kullanici_dizisi, " ==> True")
    else:
        print(kullanici_dizisi, " ==> False")

#set kullanarak çözümü
"""
if len(set(kullanici_dizisi)) <= 1:
    print("True")
else:
    print("False")
"""
